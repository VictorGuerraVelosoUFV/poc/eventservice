from app import *

if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description="Event Service CLI")
    parser.add_argument("--create-all", "-c", dest="create_all", action="store_true")
    parser.add_argument("--drop-all", "-d", dest="drop_all", action="store_true")
    args = parser.parse_args()
    if args.create_all:
        db.create_all()
    elif args.drop_all:
        db.drop_all()
    app.run(host="0.0.0.0", port=5000)
