import inspect
import sys
from datetime import datetime

from app import api, db
from flask_restplus import fields
from sqlalchemy.sql.sqltypes import String, Date, Time, DateTime, Integer, Float, Boolean


def column_to_field(tp):
    if isinstance(tp, String) or tp is String:
        return fields.String
    if isinstance(tp, Date) or tp is Date:
        return fields.Date
    if isinstance(tp, Time) or tp is Time:
        return fields.String(example=str(datetime.now().time().isoformat()))
    if isinstance(tp, DateTime) or tp is DateTime:
        return fields.DateTime
    if isinstance(tp, Integer) or tp is Integer:
        return fields.Integer
    if isinstance(tp, Float) or tp is Float:
        return fields.Float
    if isinstance(tp, Boolean) or tp is Boolean:
        return fields.Boolean(example=False)
    raise TypeError(f"Not treated column type: {tp}")


def db_to_api(blacklist=[]):
    def inner(cls):
        bl = {}
        d = {}
        for name in vars(cls):
            obj = getattr(cls, name)
            if not inspect.ismethod(obj) and name[0] != '_':
                try:
                    if isinstance(blacklist, list) and name in blacklist:
                        bl[name] = column_to_field(type(obj.type))
                    else:
                        d[name] = column_to_field(type(obj.type))
                        bl[name] = d[name]
                except (AttributeError, TypeError) as e:
                    print(e.with_traceback(None), file=sys.stderr)
        print(f"api.model({cls.__name__}, {d})")
        cls.RequestModel = api.model(f"Request{cls.__name__}", d)
        cls.ResponseModel = api.model(f"Response{cls.__name__}", bl)
        return cls
    return inner
