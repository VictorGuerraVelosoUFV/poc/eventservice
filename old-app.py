from flask import Flask, request
from flask_sqlalchemy import SQLAlchemy
from flask_restplus import Resource, Api
from flask_marshmallow import Marshmallow

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:root@localhost/mymobiconf'

api = Api(app)

db = SQLAlchemy(app)

ma = Marshmallow(app)

if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description="Event Service CLI")
    parser.add_argument("--create-all", "-c", dest="create_all", action="store_true")
    parser.add_argument("--drop-all", "-d", dest="drop_all", action="store_true")
    args = parser.parse_args()
    if args.create_all:
        db.create_all()
    elif args.drop_all:
        db.drop_all()
    app.run()
