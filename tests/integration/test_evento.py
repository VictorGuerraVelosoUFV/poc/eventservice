from http import HTTPStatus

import pytest
import requests
from flask_sqlalchemy import SQLAlchemy

from auth.AuthLevel import AuthLevel
from auth.authorize import check_authorization

credentials = {
    "username": "string",
    "password": "string"
}


@pytest.fixture()
def tear_down_deletion():
    class T:
        @classmethod
        def tear_down(cls, test_client, evento_id, auth_data):
            test_client.delete(f"/evento/{evento_id}?access_token={auth_data['id']}&uid={auth_data['userId']}",
                               follow_redirects=True)

    return T


@pytest.fixture()
def failing_auth(monkeypatch):
    class DumbResponse:
        ok = False
        status_code = HTTPStatus.UNAUTHORIZED

    monkeypatch.setattr(requests, "get", lambda *args, **kwargs: DumbResponse)
    return monkeypatch


@pytest.fixture()
def mocking_db():
    from app import app, db
    app.config['SQLALCHEMY_DATABASE_URI'] = "sqlite://"
    app.config['TESTING'] = True
    db.create_all()

    return {"app": app}


def test_event_creation_w_admin_set_failure(failing_auth, mocking_db, tear_down_deletion):
    app = mocking_db["app"]
    auth_data = {key: value for key, value in
                 requests.post("http://poc.primary157.com.br:82/api/auth_users/login", json=credentials).json().items()
                 if key in ['id', 'userId']}
    with app.test_client() as c:
        rv = c.get(f"/evento?access_token={auth_data['id']}&uid={auth_data['userId']}",
                   follow_redirects=True)
        assert rv.status_code == HTTPStatus.OK
        n_eventos = len(rv.get_json())

        novo_evento = {
            "sigla": "string",
            "nome": "string",
            "informacoes_gerais": "string",
            "data_inicio": "1999-01-01",
            "data_fim": "1999-01-01",
            "urllogo": "string",
            "local_principal": "string",
            "ativo": 0,
            "privacidade": 0
        }
        rv = c.post(f"/evento?access_token={auth_data['id']}&uid={auth_data['userId']}", json=novo_evento,
                    follow_redirects=True)
        assert rv.status_code == HTTPStatus.UNAUTHORIZED, "Authorized event creation even with admin set failure"

        rv = c.get(f"/evento?access_token={auth_data['id']}&uid={auth_data['userId']}", follow_redirects=True)
        assert n_eventos == len(rv.get_json()), "Despite what the response said, the event was created"

        failing_auth.undo()

        rv = c.post(f"/evento?access_token={auth_data['id']}&uid={auth_data['userId']}", json=novo_evento,
                    follow_redirects=True)
        assert rv.status_code == HTTPStatus.OK
        rv_json = rv.get_json()
        e_id = rv_json["id"]
        assert all(novo_evento[i] == rv_json[i] for i in novo_evento)

        novo_evento["id"] = rv_json["id"]

        rv = c.get(f"/evento?access_token={auth_data['id']}&uid={auth_data['userId']}", follow_redirects=True)
        assert n_eventos + 1 == len(rv.get_json()), "Não identificada a alteração esperada no número de eventos"

        tear_down_deletion.tear_down(c, e_id, auth_data)


def test_evento_crud(mocking_db):  # Tests integration with AuthService and UserService
    app = mocking_db["app"]
    auth_data = {key: value for key, value in
                 requests.post("http://poc.primary157.com.br:82/api/auth_users/login", json=credentials).json().items()
                 if key in ['id', 'userId']}
    with app.test_client() as c:
        rv = c.get(f"/evento?access_token={auth_data['id']}&uid={auth_data['userId']}",
                   follow_redirects=True)
        assert rv.status_code == HTTPStatus.OK
        n_eventos = len(rv.get_json())

        novo_evento = {
            "sigla": "string",
            "nome": "string",
            "informacoes_gerais": "string",
            "data_inicio": "1999-01-01",
            "data_fim": "1999-01-01",
            "urllogo": "string",
            "local_principal": "string",
            "ativo": 0,
            "privacidade": 0
        }
        rv = c.post(f"/evento?access_token={auth_data['id']}&uid={auth_data['userId']}", json=novo_evento,
                    follow_redirects=True)
        assert rv.status_code == HTTPStatus.OK
        rv_json = rv.get_json()
        assert all(novo_evento[i] == rv_json[i] for i in novo_evento)

        novo_evento["id"] = rv_json["id"]

        rv = c.get(f"/evento?access_token={auth_data['id']}&uid={auth_data['userId']}", follow_redirects=True)
        assert n_eventos + 1 == len(rv.get_json()), "Não identificada a alteração esperada no número de eventos"

        rv = c.get(f"/evento/{novo_evento['id']}?access_token={auth_data['id']}&uid={auth_data['userId']}",
                   follow_redirects=True)
        rv_json = rv.get_json()
        assert all(novo_evento[i] == rv_json[i] for i in novo_evento)
        print(novo_evento)
        rv = c.delete(f"/evento/{novo_evento['id']}?access_token={auth_data['id']}&uid={auth_data['userId']}")
        assert rv.status_code == HTTPStatus.OK

        rv = c.get(f"/evento?access_token={auth_data['id']}&uid={auth_data['userId']}", follow_redirects=True)
        assert n_eventos == len(rv.get_json()), "Não identificada a alteração esperada no número de eventos"

        rv = c.get(f"/evento/{novo_evento['id']}?access_token={auth_data['id']}&uid={auth_data['userId']}",
                   follow_redirects=True)
        assert rv.status_code == HTTPStatus.NOT_FOUND


@pytest.fixture()
def pre_creation(mocking_db):
    app = mocking_db["app"]
    auth_data = {key: value for key, value in
                 requests.post("http://poc.primary157.com.br:82/api/auth_users/login", json=credentials).json().items()
                 if key in ['id', 'userId']}
    with app.test_client() as c:
        novo_evento = {
            "sigla": "string",
            "nome": "string",
            "informacoes_gerais": "string",
            "data_inicio": "1999-01-01",
            "data_fim": "1999-01-01",
            "urllogo": "string",
            "local_principal": "string",
            "ativo": 0,
            "privacidade": 0
        }
        rv = c.post(f"/evento?access_token={auth_data['id']}&uid={auth_data['userId']}", json=novo_evento,
                    follow_redirects=True)
        e_json = rv.get_json()
        yield e_json
        c.delete(f"/evento/{e_json['id']}?access_token={auth_data['id']}&uid={auth_data['userId']}",
                 follow_redirects=True)


def test_evento_unauthorized_deletion(mocking_db, pre_creation):  # Tests integration with AuthService
    novo_evento = pre_creation
    app = mocking_db["app"]
    with app.test_client() as c:
        rv = c.get(f"/evento",
                   follow_redirects=True)
        assert rv.status_code == HTTPStatus.OK
        n_eventos = len(rv.get_json())

        rv = c.delete(f"/evento/{novo_evento['id']}")
        print(rv)
        assert rv.status_code == HTTPStatus.UNAUTHORIZED
        rv_json = rv.get_json()
        assert "message" in rv_json and rv_json["message"] == "Missing access_token and/or uid Query Params"

        rv = c.get(f"/evento/{novo_evento['id']}",
                   follow_redirects=True)

        assert rv.status_code == HTTPStatus.OK
        rv_json = rv.get_json()
        assert all(novo_evento[i] == rv_json[i] for i in novo_evento)

        rv = c.get(f"/evento",
                   follow_redirects=True)
        assert n_eventos == len(rv.get_json()), "Identificada alteração inesperada no número de eventos"


def test_evento_unauthorized_creation(mocking_db):  # Tests integration with AuthService
    app = mocking_db["app"]
    with app.test_client() as c:
        rv = c.get(f"/evento",
                   follow_redirects=True)
        assert rv.status_code == HTTPStatus.OK
        n_eventos = len(rv.get_json())

        novo_evento = {
            "sigla": "string",
            "nome": "string",
            "informacoes_gerais": "string",
            "data_inicio": "1999-01-01",
            "data_fim": "1999-01-01",
            "urllogo": "string",
            "local_principal": "string",
            "ativo": 0,
            "privacidade": 0
        }
        rv = c.post(f"/evento", json=novo_evento,
                    follow_redirects=True)
        assert rv.status_code == HTTPStatus.UNAUTHORIZED

        rv_json = rv.get_json()
        assert "message" in rv_json and rv_json["message"] == "Missing access_token and/or uid Query Params"

        rv = c.get(f"/evento", follow_redirects=True)
        assert n_eventos == len(rv.get_json()), "Identificada alteração inesperada no número de eventos"


def test_evento_authorization_unset_on_deletion(mocking_db):
    app = mocking_db["app"]
    auth_data = {key: value for key, value in
                 requests.post("http://poc.primary157.com.br:82/api/auth_users/login", json=credentials).json().items()
                 if key in ['id', 'userId']}
    novo_evento = {
        "sigla": "string",
        "nome": "string",
        "informacoes_gerais": "string",
        "data_inicio": "1999-01-01",
        "data_fim": "1999-01-01",
        "urllogo": "string",
        "local_principal": "string",
        "ativo": 0,
        "privacidade": 0
    }
    with app.test_client() as c:
        with pytest.raises((ValueError, RuntimeError)):
            check_authorization(1, auth_level=AuthLevel.ADMIN, access_token=auth_data['id'], uid=auth_data['userId'])

        rv = c.post(f"/evento?access_token={auth_data['id']}&uid={auth_data['userId']}", json=novo_evento,
                    follow_redirects=True)
        assert rv.status_code == HTTPStatus.OK
        rv_json = rv.get_json()

        novo_evento["id"] = rv_json["id"]

        assert all(novo_evento[i] == rv_json[i] for i in novo_evento)

        check_authorization(1, auth_level=AuthLevel.ADMIN, access_token=auth_data['id'], uid=auth_data['userId'])

        rv = c.delete(f"/evento/{novo_evento['id']}?access_token={auth_data['id']}&uid={auth_data['userId']}")
        assert rv.status_code == HTTPStatus.OK, f"Falhou com a mensagem: {rv.get_json()}"

        with pytest.raises((ValueError, RuntimeError)):
            check_authorization(1, auth_level=AuthLevel.ADMIN, access_token=auth_data['id'], uid=auth_data['userId'])


def test_evento_wrong_user_crud(mocking_db, tear_down_deletion):  # Tests integration with UserService
    app = mocking_db["app"]
    auth_data = {key: value for key, value in
                 requests.post("http://poc.primary157.com.br:82/api/auth_users/login", json=credentials).json().items()
                 if key in ['id', 'userId']}
    correct_credentials = {
        "username": "victorgv",
        "password": "string"
    }
    correct_auth_data = {key: value for key, value in
                         requests.post("http://poc.primary157.com.br:82/api/auth_users/login",
                                       json=correct_credentials).json().items() if key in ['id', 'userId']}
    with app.test_client() as c:
        rv = c.get(f"/evento?access_token={auth_data['id']}&uid={auth_data['userId']}",
                   follow_redirects=True)
        assert rv.status_code == HTTPStatus.OK
        n_eventos = len(rv.get_json())

        novo_evento = {
            "sigla": "string",
            "nome": "string",
            "informacoes_gerais": "string",
            "data_inicio": "1999-01-01",
            "data_fim": "1999-01-01",
            "urllogo": "string",
            "local_principal": "string",
            "ativo": 0,
            "privacidade": 0
        }
        rv = c.post(f"/evento?access_token={correct_auth_data['id']}&uid={correct_auth_data['userId']}",
                    json=novo_evento,
                    follow_redirects=True)
        assert rv.status_code == HTTPStatus.OK, f"Falhou com a mensagem: {rv.get_json()}"
        rv_json = rv.get_json()

        novo_evento["id"] = rv_json["id"]

        rv = c.get(f"/evento?access_token={auth_data['id']}&uid={auth_data['userId']}", follow_redirects=True)
        assert n_eventos + 1 == len(rv.get_json()), "Não identificada a alteração esperada no número de eventos"

        rv = c.get(f"/evento/{novo_evento['id']}?access_token={auth_data['id']}&uid={auth_data['userId']}",
                   follow_redirects=True)
        rv_json = rv.get_json()
        assert all(novo_evento[i] == rv_json[i] for i in novo_evento)

        rv = c.delete(f"/evento/{novo_evento['id']}?access_token={auth_data['id']}&uid={auth_data['userId']}")
        assert rv.status_code == HTTPStatus.UNAUTHORIZED

        rv = c.get(f"/evento/{novo_evento['id']}?access_token={auth_data['id']}&uid={auth_data['userId']}",
                   follow_redirects=True)
        assert rv.status_code == HTTPStatus.OK
        rv_json = rv.get_json()
        assert all(novo_evento[i] == rv_json[i] for i in novo_evento)

        tear_down_deletion.tear_down(c, novo_evento["id"], correct_auth_data)
