from http import HTTPStatus

import pytest
import requests
from flask_sqlalchemy import SQLAlchemy

from auth import authorize


def gen_string(size):
    import string
    import random
    return ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(size))


user_id = "52420e73-6979-466c-8e4f-0ff3c2ba9374"


@pytest.fixture()
def mocking_db():
    from app import app, db
    app.config['SQLALCHEMY_DATABASE_URI'] = "sqlite://"
    app.config['TESTING'] = True
    db.create_all()

    return {"app": app}


@pytest.fixture()
def mocking_auth(monkeypatch):
    class DumbRequest:
        ok = True

        def __str__(self):
            return repr(self)

        def __repr__(self):
            return f"{self.__class__}(ok:{self.ok},json:{self.json()})"

        @staticmethod
        def json():
            return {
                "email": "victorgv@gmail.com",
                "last_login": "2019-08-24T07:05:52.000Z",
                "is_superuser": True,
                "username": "string",
                "first_name": "string",
                "last_name": "string",
                "is_staff": True,
                "is_active": True,
                "date_joined": "2019-08-24T07:05:52.000Z",
                "id": user_id
            }

    monkeypatch.setattr(requests, "get", lambda *args, **kwargs: DumbRequest)
    monkeypatch.setattr(requests, "delete", lambda *args, **kwargs: DumbRequest)
    monkeypatch.setattr(authorize, "_authorize", lambda *args, **kwargs: DumbRequest)


def test_evento_crud(mocking_auth, mocking_db):
    app = mocking_db["app"]
    access_token = gen_string(64)
    with app.test_client() as c:
        rv = c.get(f"/evento?access_token={access_token}&uid={user_id}", follow_redirects=True)
        assert rv.status_code == HTTPStatus.OK
        n_eventos = len(rv.get_json())

        novo_evento = {
            "sigla": "string",
            "nome": "string",
            "informacoes_gerais": "string",
            "data_inicio": "1999-01-01",
            "data_fim": "1999-01-01",
            "urllogo": "string",
            "local_principal": "string",
            "ativo": 0,
            "privacidade": 0
        }
        rv = c.post(f"/evento?access_token={access_token}&uid={user_id}", json=novo_evento, follow_redirects=True)
        rv_json = rv.get_json()
        assert all(novo_evento[i] == rv_json[i] for i in novo_evento)

        novo_evento["id"] = rv_json["id"]

        rv = c.get(f"/evento?access_token={access_token}&uid={user_id}", follow_redirects=True)
        assert n_eventos + 1 == len(rv.get_json()), "Não identificada a alteração esperada no número de eventos"

        rv = c.get(f"/evento/{novo_evento['id']}?access_token={access_token}&uid={user_id}", follow_redirects=True)
        rv_json = rv.get_json()
        assert all(novo_evento[i] == rv_json[i] for i in novo_evento)

        rv = c.delete(f"/evento/{novo_evento['id']}?access_token={access_token}&uid={user_id}")
        assert rv.status_code == HTTPStatus.OK

        rv = c.get(f"/evento?access_token={access_token}&uid={user_id}", follow_redirects=True)
        assert n_eventos == len(rv.get_json()), "Não identificada a alteração esperada no número de eventos"

        rv = c.get(f"/evento/{novo_evento['id']}?access_token={access_token}&uid={user_id}", follow_redirects=True)
        assert rv.status_code == HTTPStatus.NOT_FOUND
