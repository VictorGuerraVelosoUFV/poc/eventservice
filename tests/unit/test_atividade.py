import datetime
from http import HTTPStatus

import pytest
import requests


def gen_string(size):
    import string
    import random
    return ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(size))


user_id = "52420e73-6979-466c-8e4f-0ff3c2ba9374"


@pytest.fixture()
def mocking_db():
    from app import app, db
    app.config['SQLALCHEMY_DATABASE_URI'] = "sqlite://"
    app.config['TESTING'] = True
    db.create_all()

    return {"app": app, "db": db}


@pytest.fixture()
def mocking_auth(monkeypatch):
    class DumbResponse:
        ok = True

        @staticmethod
        def json():
            return {
                "email": "victorgv@gmail.com",
                "last_login": "2019-08-24T07:05:52.000Z",
                "is_superuser": True,
                "username": "string",
                "first_name": "string",
                "last_name": "string",
                "is_staff": True,
                "is_active": True,
                "date_joined": "2019-08-24T07:05:52.000Z",
                "id": user_id
            }

    monkeypatch.setattr(requests, "get", lambda *args, **kwargs: DumbResponse)


@pytest.fixture()
def fake_required_data(mocking_db):
    from random import uniform
    from models import Evento, Local, TipoAtividade
    evento_id = int(uniform(5, 1000))
    db = mocking_db["db"]
    db.session.add(
        Evento(id=evento_id, sigla="FLISoL", nome="Festival Latino-Americano de instalação de Software Livre",
               informacoes_gerais="Evento sobre software livre", data_inicio=datetime.date.fromisoformat("2019-04-16"),
               data_fim=datetime.date.fromisoformat("2019-04-16"), local_principal="PP", ativo=True, privacidade=True))
    db.session.commit()

    local_id = int(uniform(5, 1000))
    db.session.add(Local(id=local_id, nome="UFV - Campus Florestal",
                         descricao="Campus Florestal da Universidade Fedaral de Viçosa"))
    db.session.commit()

    tipo_id = int(uniform(5, 1000))
    db.session.add(TipoAtividade(id=tipo_id, nome="Palestra"))
    db.session.commit()

    return {"evento_id": evento_id, "local_id": local_id, "tipo_id": tipo_id}


@pytest.fixture()
def tear_down_deletion():
    class T:
        @classmethod
        def tear_down(cls, test_client, atividade_id, auth_data):
            test_client.delete(f"/atividade/{atividade_id}?access_token={auth_data['id']}&uid={auth_data['userId']}",
                               follow_redirects=True)

    return T


def test_atividade_crud(mocking_auth, mocking_db, fake_required_data, tear_down_deletion):
    app = mocking_db["app"]
    access_token = gen_string(64)
    with app.test_client() as c:
        rv = c.get(f"/atividade?access_token={access_token}&uid={user_id}", follow_redirects=True)
        assert rv.status_code == HTTPStatus.OK
        n_atividades = len(rv.get_json())
        nova_atividade = {
            "nome": "string",
            "data": "2019-04-16",
            "hora_inicio": "07:05:52",
            "hora_fim": "09:32:52",
            "descricao": "string",
            "tema": "string",
            "responsavel": "string",
            "grupo": 0,
            "id_evento": fake_required_data["evento_id"],
            "id_local": fake_required_data["local_id"],
            "id_tipo": fake_required_data["tipo_id"]
        }
        rv = c.post(f"/atividade?access_token={access_token}&uid={user_id}", json=nova_atividade, follow_redirects=True)
        rv_json = rv.get_json()
        assert all(nova_atividade[i] == rv_json[i] for i in nova_atividade)

        nova_atividade["id"] = rv_json["id"]

        rv = c.get(f"/atividade?access_token={access_token}&uid={user_id}", follow_redirects=True)
        assert n_atividades + 1 == len(rv.get_json()), "Não identificada a alteração esperada no número de atividades"

        rv = c.get(f"/atividade/{nova_atividade['id']}?access_token={access_token}&uid={user_id}",
                   follow_redirects=True)
        rv_json = rv.get_json()
        assert all(nova_atividade[i] == rv_json[i] for i in nova_atividade)

        rv = c.get(f"/evento/{fake_required_data['evento_id']}/atividades?access_token={access_token}&uid={user_id}",
                   follow_redirects=True)
        assert rv.status_code == HTTPStatus.OK
        assert len(rv.get_json()) == 1

        c.delete(f"/atividade/{nova_atividade['id']}?access_token={access_token}&uid={user_id}")

        rv = c.get(f"/atividade?access_token={access_token}&uid={user_id}", follow_redirects=True)
        assert n_atividades == len(rv.get_json()), "Não identificada a alteração esperada no número de atividades"

        rv = c.get(f"/atividade/{nova_atividade['id']}?access_token={access_token}&uid={user_id}",
                   follow_redirects=True)
        assert rv.status_code == HTTPStatus.NOT_FOUND

        tear_down_deletion.tear_down(c, nova_atividade["id"], {'id': access_token, 'userId': user_id})
