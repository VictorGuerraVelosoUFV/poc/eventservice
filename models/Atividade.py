import datetime

from app import db
from decorators import add_schema, db_to_api


@add_schema
@db_to_api(blacklist=["id"])
class Atividade(db.Model):
    # __tablename__ = 'atividade'

    id = db.Column(db.Integer, name='ID', primary_key=True)  # Field name made lowercase.
    nome = db.Column(db.String(200), name='Nome')  # Field name made lowercase.
    data = db.Column(db.Date, name='Data')  # Field name made lowercase.
    hora_inicio = db.Column(db.Time, name='Hora_inicio', default=datetime.datetime.now().time())  # Field name made lowercase.
    hora_fim = db.Column(db.Time, name='Hora_fim', default=datetime.datetime.now().time())  # Field name made lowercase.
    descricao = db.Column(db.String(2500), name='Descricao', nullable=True)  # Field name made lowercase.
    tema = db.Column(db.String(200), name='Tema', nullable=True)  # Field name made lowercase.
    responsavel = db.Column(db.String(2000), name='Responsavel')  # Field name made lowercase.
    grupo = db.Column(db.Integer, name='Grupo', nullable=False, default='-1')

    id_evento = db.Column(db.Integer, db.ForeignKey("evento.ID"), name="ID_EVENTO", nullable=False)

    id_local = db.Column(db.Integer, db.ForeignKey("local.ID"), name='ID_LOCAL', nullable=False)

    id_tipo = db.Column(db.Integer, db.ForeignKey("tipo_atividade.ID"), name='ID_TIPO', nullable=True)

    def __repr__(self):
        return f"Atividade<id({self.id}),nome({self.nome}),data({self.data})," + \
               f"hora_inicio({self.hora_inicio}),hora_fim({self.hora_fim})," + \
               f"descricao({self.descricao}),tema({self.tema}),responsavel({self.responsavel})," + \
               f"grupo({self.grupo}),id_evento({self.id_evento}),id_local({self.id_local}),id_tipo({self.id_tipo})>"
