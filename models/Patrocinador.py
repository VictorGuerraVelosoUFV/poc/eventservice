from app import db
from decorators import db_to_api, add_schema


@add_schema
@db_to_api(blacklist=["id_evento", "id"])
class Patrocinador(db.Model):
    # __tablename__ = 'patrocinador'
    id = db.Column(db.Integer, name='ID', primary_key=True)  # Field name made lowercase.
    nome = db.Column(db.String(500), name='Nome')  # Field name made lowercase.
    url = db.Column(db.String(500))
    urlimagem = db.Column(db.String(500), name='urlImagem')  # Field name made lowercase.
    prioridade = db.Column(db.Integer)

    id_evento = db.Column(db.Integer, db.ForeignKey("evento.ID"), name="ID_EVENTO")
