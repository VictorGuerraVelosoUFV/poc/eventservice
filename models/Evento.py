from app import db
from decorators import db_to_api, add_schema


@add_schema
@db_to_api(blacklist=["id"])
class Evento(db.Model):
    # __tablename__ = 'evento'
    id = db.Column(db.Integer, name='ID', primary_key=True)  # Field name made lowercase.
    sigla = db.Column(db.String(20), db_column='Sigla')  # Field name made lowercase.
    nome = db.Column(db.String(100))
    informacoes_gerais = db.Column(db.String(5000))
    data_inicio = db.Column(db.Date, name='Data_inicio')  # Field name made lowercase.
    data_fim = db.Column(db.Date, name='Data_fim')  # Field name made lowercase.
    urllogo = db.Column(db.String(400), name='urlLogo', nullable=True)  # Field name made lowercase.
    local_principal = db.Column(db.String(140))
    ativo = db.Column(db.Boolean, name='Ativo')  # Field name made lowercase.
    privacidade = db.Column(db.Boolean, name='Privacidade')

    atividade = db.relationship('Atividade', cascade="all", backref=db.backref('evento', lazy=True))

    informacao_evento = db.relationship('InformacaoEvento', cascade="all", backref=db.backref('evento', lazy=True))

    noticia = db.relationship('Noticia', cascade="all", backref=db.backref('evento', lazy=True))

    patrocinador = db.relationship('Patrocinador', cascade="all", backref=db.backref('evento', lazy=True))
