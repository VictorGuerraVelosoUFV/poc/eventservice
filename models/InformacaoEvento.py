from app import db
from decorators import add_schema, db_to_api


@add_schema
@db_to_api(blacklist=["id_evento", "id"])
class InformacaoEvento(db.Model):
    # __tablename__ = 'informacao_evento'
    id = db.Column(db.Integer, name='ID', primary_key=True)  # Field name made lowercase.

    id_evento = db.Column(db.Integer, db.ForeignKey("evento.ID"), name="ID_EVENTO")

    site = db.Column(db.String(200), name='SITE', nullable=True)  # Field name made lowercase.
    email = db.Column(db.String(80), name='EMAIL', nullable=True)  # Field name made lowercase.
    telefone = db.Column(db.String(20), name='TELEFONE', nullable=True)  # Field name made lowercase.
