from app import db
from decorators import db_to_api, add_schema


@add_schema
@db_to_api(blacklist=["id"])
class Local(db.Model):
    # __tablename__ = 'local'
    id = db.Column(db.Integer, name='ID', primary_key=True)  # Field name made lowercase.
    nome = db.Column(db.String(200), name='Nome')  # Field name made lowercase.
    descricao = db.Column(db.String(140), name='Descricao', nullable=True)  # Field name made lowercase.
    logradouro = db.Column(db.String(100), name='Logradouro', nullable=True)  # Field name made lowercase.
    cep = db.Column(db.String(20), name='CEP', nullable=True)  # Field name made lowercase.
    bairro = db.Column(db.String(50), name='Bairro', nullable=True)  # Field name made lowercase.
    numero = db.Column(db.Integer, name='Numero', nullable=True)  # Field name made lowercase.
    cidade = db.Column(db.String(50), name='Cidade', nullable=True)  # Field name made lowercase.
    estado = db.Column(db.String(50), name='Estado', nullable=True)  # Field name made lowercase.
    pais = db.Column(db.String(50), name='Pais', nullable=True)  # Field name made lowercase.
    coordx = db.Column(db.Float, name='Coordx', nullable=True)  # Field name made lowercase.
    coordy = db.Column(db.Float, name='Coordy', nullable=True)  # Field name made lowercase.
    complemento = db.Column(db.String(50), name='Complemento', nullable=True)  # Field name made lowercase.

    atividade = db.relationship('Atividade', backref=db.backref('local', lazy=True))
