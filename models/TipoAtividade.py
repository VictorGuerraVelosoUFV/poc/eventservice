from app import db
from decorators import db_to_api, add_schema


@add_schema
@db_to_api(blacklist=["id"])
class TipoAtividade(db.Model):
    # __tablename__ = 'atividade'

    id = db.Column(db.Integer, name='ID', primary_key=True)  # Field name made lowercase.
    nome = db.Column(db.String(50), name='Nome')  # Field name made lowercase.

    atividade = db.relationship('Atividade', backref=db.backref('tipo', lazy=True))
