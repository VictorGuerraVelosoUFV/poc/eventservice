from app import db
from decorators import db_to_api, add_schema


@add_schema
@db_to_api(blacklist=["id_evento", "id"])
class Noticia(db.Model):
    # __tablename__ = 'noticia'
    id = db.Column(db.Integer, name='ID_NOTICIA', primary_key=True)  # Field name made lowercase.

    id_evento = db.Column(db.Integer, db.ForeignKey("evento.ID"), name="ID_EVENTO")

    hora = db.Column(db.DateTime)
    titulo = db.Column(db.String(200))
    corpo = db.Column(db.String(5000))
