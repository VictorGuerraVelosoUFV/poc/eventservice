import sys
from http import HTTPStatus

import requests

from auth.AuthLevel import AuthLevel


def check_authorization(event, auth_level=AuthLevel.PARTICIPANTE, base_url="poc.primary157.com.br:84", **kwargs):
    assert kwargs["access_token"] is not None
    assert kwargs["uid"] is not None
    return _authorize(event, auth_level=auth_level, base_url=base_url, req_method=requests.get, **kwargs)


def set_authorization(event, auth_level=AuthLevel.PARTICIPANTE, base_url="poc.primary157.com.br:84", **kwargs):
    assert kwargs["access_token"] is not None
    assert kwargs["uid"] is not None
    return _authorize(event, auth_level=auth_level, base_url=base_url, req_method=requests.post, **kwargs)


def unset_authorizations(event, base_url="poc.primary157.com.br:84", **kwargs):
    assert kwargs["access_token"] is not None
    assert kwargs["uid"] is not None
    uid = kwargs["uid"]
    token = kwargs["access_token"]
    response = requests.delete(f"http://{base_url}/evento/{event}?access_token={token}&uid={uid}")
    print(f"http://{base_url}/evento/{event}?access_token={token}&uid={uid}")
    if response.ok:
        return True
    print(f"response.status_code = {response.status_code}")
    if response.status_code == HTTPStatus.UNAUTHORIZED:
        raise ValueError(
            f"Authentication Failed: wrong access_token ({kwargs['access_token']}) and uid({kwargs['uid']}) pair!")
    else:
        raise RuntimeError("Something went wrong during authorization check!")


def _authorize(event, auth_level=AuthLevel.PARTICIPANTE, base_url="poc.primary157.com.br:84", req_method=requests.get,
               **kwargs):
    token = kwargs["access_token"]
    uid = kwargs["uid"]
    print(f"http://{base_url}/evento/{event}/{auth_level.value}/{uid}?access_token={token}&uid={uid}", file=sys.stderr)
    response = req_method(f"http://{base_url}/evento/{event}/{auth_level.value}/{uid}?access_token={token}&uid={uid}")
    if response.ok:
        return True
    elif response.status_code == HTTPStatus.UNAUTHORIZED:
        raise ValueError(
            f"Authentication Failed: wrong access_token ({kwargs['access_token']}) and uid({kwargs['uid']}) pair!")
    else:
        raise RuntimeError("Something went wrong during authorization check!")
