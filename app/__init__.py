from flask import Flask, request
from flask_sqlalchemy import SQLAlchemy
from flask_restplus import Resource, Api, fields
app = Flask(__name__)


api = Api(app)

db = SQLAlchemy(app)


def register_namespaces():
    from resources import local_ns, evento_ns, atividade_ns, tipo_atividade_ns

    api.add_namespace(evento_ns)
    api.add_namespace(atividade_ns)
    api.add_namespace(local_ns)
    api.add_namespace(tipo_atividade_ns)


register_namespaces()
