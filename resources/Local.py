import sys

import flask
from flask_restplus import abort, Namespace
from sqlalchemy.exc import DatabaseError

from app import db, Resource, request
from http import HTTPStatus
from models import Local
from auth.authenticate import authenticate_or_abort

local_ns = Namespace(name="local", description="Rotas relacionadas a locals", path="/local")


@local_ns.route("/")
class LocaisResource(Resource):
    @local_ns.marshal_with(Local.ResponseModel)
    def get(self):
        local = Local.query.all()
        authenticate_or_abort(flask.request.args.get, abort)
        return local

    @local_ns.expect(Local.RequestModel)
    @local_ns.marshal_with(Local.ResponseModel)
    @local_ns.response(code=HTTPStatus.UNAUTHORIZED, description="")
    @local_ns.response(code=HTTPStatus.BAD_REQUEST, description="")
    def post(self):
        local_json = request.get_json()
        authenticate_or_abort(flask.request.args.get, abort)
        local, err = Local.Schema().load(local_json, session=db.session)
        if err:
            print(err, file=sys.stderr)
            abort(HTTPStatus.BAD_REQUEST, err)
        db.session.add(local)
        try:
            db.session.commit()
        except DatabaseError as e:
            print(e, file=sys.stderr)
            abort(HTTPStatus.BAD_REQUEST)
        return Local.query.order_by(Local.id.desc()).first()


@local_ns.route("/<id>")
class LocalResource(Resource):
    @local_ns.marshal_with(Local.ResponseModel)
    def get(self, id):
        local = Local.query.get(id)
        authenticate_or_abort(flask.request.args.get, abort)
        if local is None:
            abort(HTTPStatus.NOT_FOUND)
        return local

    def delete(self, id):  # This endpoint should not exist (There is no way of checking authorization)
        abort(HTTPStatus.NOT_IMPLEMENTED, "This endpoint is a WIP (Working In Progress). Come back later!")
        local = Local.query.get(id)
        authenticate_or_abort(flask.request.args.get, abort)
        if local is None:
            abort(HTTPStatus.NOT_FOUND)
        db.session.delete(local)
        try:
            db.session.commit()
        except DatabaseError as e:
            print(e, file=sys.stderr)
            abort(HTTPStatus.BAD_REQUEST)
