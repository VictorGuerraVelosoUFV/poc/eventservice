import sys

import flask
from flask_restplus import abort, Namespace
from sqlalchemy.exc import DatabaseError

from app import db, Resource, request
from http import HTTPStatus

from auth.AuthLevel import AuthLevel
from auth.authorize import check_authorization
from models import Atividade, TipoAtividade, Evento, Local
from auth.authenticate import authenticate_or_abort

atividade_ns = Namespace(name="atividade", description="Rotas relacionadas a atividades", path="/atividade")


@atividade_ns.route('/<id>')
class AtividadeResource(Resource):
    @atividade_ns.marshal_with(Atividade.ResponseModel)
    @atividade_ns.response(code=HTTPStatus.NOT_FOUND, description="Nenhuma atividade registrada com esse id")
    def get(self, id):
        atividade = Atividade.query.get(id)
        token, uid = authenticate_or_abort(flask.request.args.get, abort)
        if atividade is None:
            return abort(HTTPStatus.NOT_FOUND)
        try:
            check_authorization(atividade.id_evento, AuthLevel.PARTICIPANTE, access_token=token, uid=uid)
        except (ValueError, AssertionError) as e:
            abort(HTTPStatus.UNAUTHORIZED, e)
        except RuntimeError as e:
            abort(HTTPStatus.INTERNAL_SERVER_ERROR, str(e))
        return atividade

    def delete(self, id):
        atividade = Atividade.query.get(id)
        token, uid = authenticate_or_abort(flask.request.args.get, abort)
        if atividade is None:
            return abort(HTTPStatus.NOT_FOUND)
        evento_id = atividade.evento.id
        try:
            check_authorization(evento_id, AuthLevel.ADMIN, access_token=token, uid=uid)
        except (ValueError, AssertionError) as e:
            abort(HTTPStatus.UNAUTHORIZED, e)
        except RuntimeError:
            abort(HTTPStatus.INTERNAL_SERVER_ERROR)
        db.session.delete(atividade)
        db.session.commit()


@atividade_ns.route('/<id>/tipo')
class AtividadeTipoResource(Resource):
    @atividade_ns.marshal_with(TipoAtividade.ResponseModel)
    @atividade_ns.response(code=HTTPStatus.NOT_FOUND, description="Nenhuma atividade registrada com esse id")
    def get(self, id):
        atividade = Atividade.query.get(id)
        if atividade is None:
            return abort(HTTPStatus.NOT_FOUND)
        return atividade.tipo


@atividade_ns.route("/")
class AtividadesResource(Resource):
    @atividade_ns.marshal_with(Atividade.ResponseModel)
    def get(self):
        atividades = Atividade.query.all()
        authenticate_or_abort(flask.request.args.get, abort)
        if atividades is None:
            return abort(HTTPStatus.NOT_FOUND)
        return atividades

    @atividade_ns.expect(Atividade.RequestModel)
    @atividade_ns.marshal_with(Atividade.ResponseModel)
    @atividade_ns.response(code=HTTPStatus.UNAUTHORIZED, description="Operação não autorizada. Deve estar autenticado!")
    @atividade_ns.response(code=HTTPStatus.BAD_REQUEST, description="Requisição inválida/incorreta.")
    def post(self):
        atividade_json = request.get_json()
        print(f"atividade_json: {atividade_json}", file=sys.stderr)
        evento = db.session.query(Evento).get(atividade_json["id_evento"])
        local = db.session.query(Local).get(atividade_json["id_local"])
        tipo = db.session.query(TipoAtividade).get(atividade_json["id_tipo"])
        print(f"local: {local}", file=sys.stderr)
        print(f"tipo: {tipo}", file=sys.stderr)
        print(f"evento: {evento}", file=sys.stderr)
        token, uid = authenticate_or_abort(flask.request.args.get, abort)
        try:
            check_authorization(atividade_json["id_evento"], AuthLevel.ADMIN, access_token=token, uid=uid)
        except (ValueError, AssertionError) as e:
            abort(HTTPStatus.UNAUTHORIZED, e)
        except RuntimeError:
            abort(HTTPStatus.INTERNAL_SERVER_ERROR)
        atividade, err = Atividade.Schema().load(atividade_json)
        if err:
            abort(HTTPStatus.BAD_REQUEST, err)
        try:
            print(type(atividade))
            print(err)
            atividade.evento = evento
            atividade.local = local
            atividade.tipo = tipo
        except Exception as e:
            print(e, file=sys.stderr)
            abort(HTTPStatus.BAD_REQUEST, e)
        print(atividade)
        db.session.add(atividade)
        try:
            db.session.commit()
        except DatabaseError as e:
            print(e, file=sys.stderr)
            abort(HTTPStatus.BAD_REQUEST)
        return Atividade.query.order_by(Atividade.id.desc()).first()


@atividade_ns.route("/<id>/local")
class LocalAtividadeResource(Resource):
    @atividade_ns.marshal_with(Local.ResponseModel)
    @atividade_ns.response(code=HTTPStatus.UNAUTHORIZED, description="Operação não autorizada. Deve estar autenticado!")
    def get(self, id):
        atividade = Atividade.query.get(id)
        token, uid = authenticate_or_abort(flask.request.args.get, abort)
        if atividade is None:
            return abort(HTTPStatus.NOT_FOUND)
        evento_id = atividade.evento.id
        try:
            check_authorization(evento_id, AuthLevel.PARTICIPANTE, access_token=token, uid=uid)
        except (ValueError, AssertionError) as e:
            abort(HTTPStatus.UNAUTHORIZED, e)
        except RuntimeError:
            abort(HTTPStatus.INTERNAL_SERVER_ERROR)
        return atividade.local
