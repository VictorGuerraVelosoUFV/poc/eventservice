import sys
from http import HTTPStatus

import flask
from flask import request
from flask_restplus import Namespace, Resource, abort
from sqlalchemy.exc import DatabaseError

from app import db
from auth.authenticate import authenticate_or_abort
from models import TipoAtividade, Atividade

tipo_atividade_ns = Namespace(name="tipo_atividade", description="Rotas relacionadas a atividades",
                              path="/atividade/tipo")


@tipo_atividade_ns.route('/<id>')
class TipoAtividadeResource(Resource):
    @tipo_atividade_ns.marshal_with(TipoAtividade.ResponseModel)
    @tipo_atividade_ns.response(code=HTTPStatus.NOT_FOUND,
                                description="Nenhum tipo de atividade registrado com esse id")
    def get(self, id):
        tipo = TipoAtividade.query.get(id)
        if tipo is None:
            return abort(HTTPStatus.NOT_FOUND)
        return tipo

    def delete(self, id):  # This endpoint should not exist (There is no way of checking authorization)
        abort(HTTPStatus.NOT_IMPLEMENTED, "This endpoint is a WIP (Working In Progress). Come back later!")
        tipo = TipoAtividade.query.get(id)
        authenticate_or_abort(flask.request.args.get, abort)
        if tipo is None:
            return abort(HTTPStatus.NOT_FOUND)
        db.session.delete(tipo)
        db.session.commit()


@tipo_atividade_ns.route('/')
class TiposAtividadeResource(Resource):
    @tipo_atividade_ns.marshal_with(TipoAtividade.ResponseModel)
    @tipo_atividade_ns.response(code=HTTPStatus.NOT_FOUND, description="Nenhum tipo de atividade registrado")
    def get(self):
        tipos = TipoAtividade.query.all()
        if tipos is None:
            return abort(HTTPStatus.NOT_FOUND)
        return tipos

    @tipo_atividade_ns.expect(TipoAtividade.RequestModel)
    @tipo_atividade_ns.marshal_with(TipoAtividade.ResponseModel)
    @tipo_atividade_ns.response(code=HTTPStatus.UNAUTHORIZED, description="")
    @tipo_atividade_ns.response(code=HTTPStatus.BAD_REQUEST, description="")
    def post(self):  # There is no way of checking authorization: so it will be public
        tipo_atividade_json = request.get_json()
        tipo_atividade, err = TipoAtividade.Schema().load(tipo_atividade_json, session=db.session)
        authenticate_or_abort(flask.request.args.get, abort)
        # check authorization should be here
        if err:
            abort(HTTPStatus.BAD_REQUEST, err)
        db.session.add(tipo_atividade)
        try:
            db.session.commit()
        except DatabaseError as e:
            print(e, file=sys.stderr)
            abort(HTTPStatus.BAD_REQUEST)
        return TipoAtividade.query.order_by(TipoAtividade.id.desc()).first()
