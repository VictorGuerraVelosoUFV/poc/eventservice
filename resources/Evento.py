import sys

import flask
from flask_restplus import abort, Namespace
from sqlalchemy.exc import DatabaseError

from app import db, Resource, request
from http import HTTPStatus

from auth.AuthLevel import AuthLevel
from auth.authorize import set_authorization, check_authorization, unset_authorizations
from models import Evento, InformacaoEvento, Noticia, Patrocinador, Atividade
from auth.authenticate import authenticate_or_abort

evento_ns = Namespace(name="evento", description="Rotas relacionadas a eventos", path="/evento")


@evento_ns.route("/")
class EventosResource(Resource):
    @evento_ns.marshal_with(Evento.ResponseModel)
    def get(self):
        return Evento.query.all()

    @evento_ns.expect(Evento.RequestModel)
    @evento_ns.marshal_with(Evento.ResponseModel)
    @evento_ns.response(code=HTTPStatus.UNAUTHORIZED, description="")
    @evento_ns.response(code=HTTPStatus.BAD_REQUEST, description="")
    def post(self):
        evento_json = request.get_json()
        token, uid = authenticate_or_abort(flask.request.args.get, abort)
        evento, err = Evento.Schema().load(evento_json, session=db.session)
        if err:
            abort(HTTPStatus.BAD_REQUEST, err)
        db.session.add(evento)
        try:
            db.session.commit()
        except DatabaseError as e:
            print(e, file=sys.stderr)
            abort(HTTPStatus.BAD_REQUEST)
        evento = Evento.query.order_by(Evento.id.desc()).first()
        try:
            set_authorization(evento.id, AuthLevel.ADMIN, access_token=token, uid=uid)
            return evento
        except (ValueError, RuntimeError, AssertionError) as e:
            print(f"Authorization error: {e}")
            db.session.delete(evento)
            db.session.commit()
            abort(HTTPStatus.UNAUTHORIZED)


@evento_ns.route('/<id>')
class EventoResource(Resource):
    @evento_ns.marshal_with(Evento.ResponseModel)
    @evento_ns.response(code=HTTPStatus.NOT_FOUND, description="Não foi possível encontrar o evento")
    def get(self, id):
        evento = Evento.query.get(id)
        if evento is None:
            return abort(HTTPStatus.NOT_FOUND)
        return evento

    def delete(self, id):
        evento = Evento.query.get(id)
        token, uid = authenticate_or_abort(flask.request.args.get, abort)
        try:
            check_authorization(id, AuthLevel.ADMIN, access_token=token, uid=uid)
        except (ValueError, AssertionError) as e:
            abort(HTTPStatus.UNAUTHORIZED, e)
        except RuntimeError:
            abort(HTTPStatus.INTERNAL_SERVER_ERROR)
        if evento is None:
            print("Evento não encontrado")
            abort(HTTPStatus.NOT_FOUND)
        db.session.delete(evento)
        db.session.commit()
        try:
            unset_authorizations(id, access_token=token, uid=uid)
        except (ValueError, AssertionError) as e:
            abort(HTTPStatus.UNAUTHORIZED, e)
        except RuntimeError:
            abort(HTTPStatus.INTERNAL_SERVER_ERROR)


#######################################################################################################################
# Endpoints Informação Evento
#######################################################################################################################


@evento_ns.route("/<id_evento>/info")
class InformacaoEventosResource(Resource):
    @evento_ns.marshal_with(InformacaoEvento.ResponseModel)
    def get(self, id_evento):
        infos = InformacaoEvento.query.filter_by(id_evento=id_evento).all()
        authenticate_or_abort(flask.request.args.get, abort)
        return infos

    @evento_ns.expect(InformacaoEvento.RequestModel)
    @evento_ns.marshal_with(InformacaoEvento.ResponseModel)
    @evento_ns.response(code=HTTPStatus.UNAUTHORIZED, description="")
    @evento_ns.response(code=HTTPStatus.BAD_REQUEST, description="")
    def post(self, id_evento):
        informacao_evento_json = request.get_json()
        informacao_evento_json["id_evento"] = id_evento
        authenticate_or_abort(flask.request.args.get, abort)
        token, uid = authenticate_or_abort(flask.request.args.get, abort)
        try:
            check_authorization(id_evento, AuthLevel.ADMIN, access_token=token, uid=uid)
        except (ValueError, AssertionError) as e:
            abort(HTTPStatus.UNAUTHORIZED, e)
        except RuntimeError:
            abort(HTTPStatus.INTERNAL_SERVER_ERROR)
        informacao_evento, err = InformacaoEvento.Schema().load(informacao_evento_json)
        if err:
            abort(HTTPStatus.BAD_REQUEST, err)
        informacao_evento.evento = Evento.query.get(id_evento)
        db.session.add(informacao_evento)
        try:
            db.session.commit()
        except DatabaseError as e:
            print(e, file=sys.stderr)
            abort(HTTPStatus.BAD_REQUEST)
        return InformacaoEvento.query.order_by(InformacaoEvento.id.desc()).first()


@evento_ns.route("/info/<id>")
class InformacaoEventoResource(Resource):
    @evento_ns.marshal_with(InformacaoEvento.ResponseModel)
    def get(self, id):
        infos = InformacaoEvento.query.get(id)
        authenticate_or_abort(flask.request.args.get, abort)
        if infos is None:
            return abort(HTTPStatus.NOT_FOUND)
        return infos

    def delete(self, id):
        infos = InformacaoEvento.query.get(id)
        token, uid = authenticate_or_abort(flask.request.args.get, abort)
        if infos is not None:
            try:
                check_authorization(infos.id_evento, AuthLevel.ADMIN, access_token=token, uid=uid)
            except (ValueError, AssertionError) as e:
                abort(HTTPStatus.UNAUTHORIZED, e)
            except RuntimeError:
                abort(HTTPStatus.INTERNAL_SERVER_ERROR)
        else:
            print("Informação de evento não encontrada")
            abort(HTTPStatus.NOT_FOUND)
        db.session.delete(infos)
        db.session.commit()


#######################################################################################################################
# Endpoints Noticia
#######################################################################################################################

@evento_ns.route("/<id_evento>/noticia")
class NoticiasResource(Resource):
    @evento_ns.marshal_with(Noticia.ResponseModel)
    def get(self, id_evento):
        noticia = Noticia.query.filter_by(id_evento=id_evento).all()
        token, uid = authenticate_or_abort(flask.request.args.get, abort)
        try:
            check_authorization(id_evento, AuthLevel.PARTICIPANTE, access_token=token, uid=uid)
        except (ValueError, AssertionError) as e:
            abort(HTTPStatus.UNAUTHORIZED, e)
        except RuntimeError:
            abort(HTTPStatus.INTERNAL_SERVER_ERROR)
        return noticia

    @evento_ns.expect(Noticia.RequestModel)
    @evento_ns.marshal_with(Noticia.ResponseModel)
    @evento_ns.response(code=HTTPStatus.UNAUTHORIZED, description="")
    @evento_ns.response(code=HTTPStatus.BAD_REQUEST, description="")
    def post(self, id_evento):
        noticia_json = request.get_json()
        noticia_json["id_evento"] = id_evento
        token, uid = authenticate_or_abort(flask.request.args.get, abort)
        try:
            check_authorization(id_evento, AuthLevel.ADMIN, access_token=token, uid=uid)
        except (ValueError, AssertionError) as e:
            abort(HTTPStatus.UNAUTHORIZED, e)
        except RuntimeError:
            abort(HTTPStatus.INTERNAL_SERVER_ERROR)
        noticia, err = Noticia.Schema().load(noticia_json)
        if err:
            abort(HTTPStatus.BAD_REQUEST, err)
        noticia.evento = Evento.query.get(id_evento)
        db.session.add(noticia)
        try:
            db.session.commit()
        except DatabaseError as e:
            print(e, file=sys.stderr)
            abort(HTTPStatus.BAD_REQUEST)
        return Noticia.query.order_by(Noticia.id.desc()).first()


@evento_ns.route("/noticia/<id>")
class NoticiaResource(Resource):
    @evento_ns.marshal_with(Noticia.ResponseModel)
    def get(self, id):
        noticia = Noticia.query.get(id)
        authenticate_or_abort(flask.request.args.get, abort)
        if noticia is None:
            return abort(HTTPStatus.NOT_FOUND)
        return noticia

    def delete(self, id):
        noticia = Noticia.query.get(id)
        token, uid = authenticate_or_abort(flask.request.args.get, abort)
        try:
            check_authorization(noticia.id_evento, AuthLevel.ADMIN, access_token=token, uid=uid)
        except (ValueError, AssertionError) as e:
            abort(HTTPStatus.UNAUTHORIZED, e)
        except RuntimeError:
            abort(HTTPStatus.INTERNAL_SERVER_ERROR)
        if noticia is None:
            print("Noticia não encontrada")
            abort(HTTPStatus.NOT_FOUND)
        db.session.delete(noticia)
        db.session.commit()


#######################################################################################################################
# Endpoints Patrocinador
#######################################################################################################################

@evento_ns.route("/<id_evento>/patrocinador")
class PatrocinadorsResource(Resource):
    @evento_ns.marshal_with(Patrocinador.ResponseModel)
    def get(self, id_evento):
        patrocinador = Patrocinador.query.filter_by(id_evento=id_evento).all()
        token, uid = authenticate_or_abort(flask.request.args.get, abort)
        try:
            check_authorization(id_evento, AuthLevel.PARTICIPANTE, access_token=token, uid=uid)
        except (ValueError, AssertionError) as e:
            abort(HTTPStatus.UNAUTHORIZED, e)
        except RuntimeError:
            abort(HTTPStatus.INTERNAL_SERVER_ERROR)
        return patrocinador

    @evento_ns.expect(Patrocinador.RequestModel)
    @evento_ns.marshal_with(Patrocinador.ResponseModel)
    @evento_ns.response(code=HTTPStatus.UNAUTHORIZED, description="")
    @evento_ns.response(code=HTTPStatus.BAD_REQUEST, description="")
    def post(self, id_evento):
        patrocinador_json = request.get_json()
        patrocinador_json["id_evento"] = id_evento
        token, uid = authenticate_or_abort(flask.request.args.get, abort)
        try:
            check_authorization(id_evento, AuthLevel.ADMIN, access_token=token, uid=uid)
        except (ValueError, AssertionError) as e:
            abort(HTTPStatus.UNAUTHORIZED, e)
        except RuntimeError:
            abort(HTTPStatus.INTERNAL_SERVER_ERROR)

        patrocinador, err = Patrocinador.Schema().load(patrocinador_json)
        if err:
            abort(HTTPStatus.BAD_REQUEST, err)
        patrocinador.evento = Evento.query.get(id_evento)
        db.session.add(patrocinador)
        try:
            db.session.commit()
        except DatabaseError as e:
            print(e, file=sys.stderr)
            abort(HTTPStatus.BAD_REQUEST)
        return Patrocinador.query.order_by(Patrocinador.id.desc()).first()


@evento_ns.route("/patrocinador/<id>")
class PatrocinadorResource(Resource):
    @evento_ns.marshal_with(Patrocinador.ResponseModel)
    def get(self, id):
        patrocinador = Patrocinador.query.get(id)
        authenticate_or_abort(flask.request.args.get, abort)
        if patrocinador is None:
            return abort(HTTPStatus.NOT_FOUND)
        return patrocinador

    def delete(self, id):
        patrocinador = Patrocinador.query.get(id)
        token, uid = authenticate_or_abort(flask.request.args.get, abort)
        try:
            check_authorization(patrocinador.id_evento, AuthLevel.ADMIN, access_token=token, uid=uid)
        except (ValueError, AssertionError) as e:
            abort(HTTPStatus.UNAUTHORIZED, e)
        except RuntimeError:
            abort(HTTPStatus.INTERNAL_SERVER_ERROR)
        if patrocinador is None:
            print("Patrocinador não encontrado")
            abort(HTTPStatus.NOT_FOUND)
        db.session.delete(patrocinador)
        db.session.commit()


#######################################################################################################################
# Endpoints Atividades de Evento
#######################################################################################################################

@evento_ns.route("/<id_evento>/atividades")
class EventoAtividadesResource(Resource):
    @evento_ns.marshal_with(Atividade.ResponseModel)
    def get(self, id_evento):
        atividades = Atividade.query.filter_by(id_evento=id_evento).all()
        token, uid = authenticate_or_abort(flask.request.args.get, abort)
        try:
            check_authorization(id_evento, AuthLevel.PARTICIPANTE, access_token=token, uid=uid)
        except (ValueError, AssertionError) as e:
            abort(HTTPStatus.UNAUTHORIZED, e)
        except RuntimeError as e:
            abort(HTTPStatus.INTERNAL_SERVER_ERROR, str(e))
        if atividades is None:
            abort(HTTPStatus.NOT_FOUND)
        return atividades
