from .Evento import EventoResource, EventosResource, InformacaoEventoResource, InformacaoEventosResource
from .Evento import evento_ns

from .Atividade import AtividadeResource, AtividadesResource, AtividadeTipoResource
from .Atividade import atividade_ns

from .Local import LocaisResource, LocalResource
from .Local import local_ns

from .TipoAtividade import TipoAtividadeResource, TiposAtividadeResource
from .TipoAtividade import tipo_atividade_ns